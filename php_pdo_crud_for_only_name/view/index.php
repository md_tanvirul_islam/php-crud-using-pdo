<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index_page</title>
</head>
<body>
    <h1>List of Users</h1>
    <h2><a style="text-decoration:none;color: greenyellow" href="create.php"> Click Here to add User</a></h2>

    <?php
    include("../vendor/autoload.php");
    use PDO_NAME\User;
    $user_obj = new User\USER();
    $data = $user_obj->index();
//    echo"<pre>";
//    print_r($data);
//    echo"</pre>";

    ?>
    <table border="1" cellpadding = "5px" >
        <tr>
            <th>No.</th>
            <th>Id</th>
            <th>Name</th>
            <th>Actions</th>
        </tr>
    <?php
    if(count($data)>0){
    $i= 0;
    foreach($data as $user_Data)
    {

    ?>
        <tr>
            <td><?=++$i?></td>
            <td><?php echo $user_Data["ID"]?></td>
            <td><?php echo $user_Data["NAME"]?></td>
            <td><a href="show.php?id=<?=$user_Data["ID"]?>">Edit</a> <a href="delete.php?id=<?=$user_Data["ID"]?>">Delete</a></td>
        <tr>
            <?php
            }}
    else{
        echo " <h4 style='color: red'>No Record Found</h4>";
    }
            ?>
    </table>
</body>
</html>