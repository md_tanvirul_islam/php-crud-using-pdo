<?php
namespace PDO_NAME\User;
use PDO;
use PDOException;

class USER
{
    public $inputname;
    public $conn;

    public function __construct()
    {
        $servername = "localhost";
        $username = "root";
        $password = "";

        try {
            $this->conn = new PDO("mysql:host=$servername;dbname=php_crud", $username, $password);
            // set the PDO error mode to exception
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          //  echo "Connected successfully";
        } catch(PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }
    public function store()
    {
//        echo "<pre>";
//        var_dump($this->inputname);
//        echo "</pre>";
        $query ="INSERT INTO name(NAME) VALUES(:v1)";

        $stmt =$this->conn->prepare($query);

        $stmt->execute(array(
            ':v1' => $this->inputname

        ));

    }
    public function index()
    {
        try{
            $query = "SELECT * FROM NAME ORDER BY ID ASC ";

            $stmt = $this->conn->query($query);

            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $data;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }

    }

    public function show($id)
    {
        
        try{
            $query = "SELECT * FROM NAME WHERE ID =$id";

            $stmt = $this->conn->query($query);

            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            return $data;
        }
        catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function update($id,$name)
    {
        try {
            $query ="UPDATE name SET NAME=:v1  where id= :id";

            $stmt = $this->conn->prepare($query);

            $stmt ->execute(array(
                ':id'  =>$id ,
                ':v1' => $name));


        }catch(PDOException $e){
            echo $e->getMessage();
        }


    }

    public function delete($id)
    {
        try {
          //  echo $id;
            $query ="delete from name where ID=:id";
            $stmt = $this->conn->prepare($query);
            $stmt ->execute(array(
                ':id' =>$id,
                ));


        }catch(PDOException $e){
            echo $e->getMessage();
        }

    }

}
?>