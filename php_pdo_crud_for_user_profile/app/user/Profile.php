<?php


namespace PROFILE\user;
use PDO;
use PDOException;


class Profile
{
    public $conn;
    public $uname;
    public $uemail ;
    public $upassword ;
    public $ubirthdate ;
    public $uphone ;
    public $uaddress ;
    public $uedu_insti ;
    public $uclass ;
    public $usubject ;
    public $uphoto ;
    public $ubio ;

    public function __construct()
    {
        $servername = "localhost";
        $username = "root";
        $password = "";

        try {
            $this->conn = new PDO("mysql:host=$servername;dbname=php_crud", $username, $password);
            // set the PDO error mode to exception
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //  echo "Connected successfully";
        } catch(PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }

    }
    public function setdata($data)
    {

        $this->uname =$data["uname"];
        $this->uemail =$data["uemail"];
        $this->upassword =$data["upassword"];
        $this->ubirthdate =$data["ubirthdate"];
        $this->uphone =$data["uphone"];
        $this->uaddress =$data["uaddress"];
        $this->uedu_insti =$data["uedu_insti"];
        $this->uclass =$data["uclass"];
        $this->usubject =$data["usubject"];
        $this->ubio =$data["ubio"];

    }
    public function photo($file_data,$dir)
    {
//        echo "<pre>";
//        print_r($file_data);
//        echo "</pre>";

        $this->uphoto =time().$file_data["name"];
        $target_dir = $dir;
//        $target_file = $target_dir . basename($file_data["name"]);
//        echo $target_dir.$this->uphoto;
        $photo_name = $this->uphoto;
        move_uploaded_file($file_data["tmp_name"],$target_dir.$photo_name);


    }
    public function photoUpdate($file_data,$dir,$id)
    {
        try {
            $this->uphoto =time().$file_data["name"];
            $target_dir = $dir;
            $photo_name = $this->uphoto;
            $query ="UPDATE users SET photo=:photo  where id=".$id;

//            echo $this->uname;
//            echo $this->ubio;

            $stmt =$this->conn->prepare($query);

            $stmt->execute(array(
                ':photo' => $this->uphoto,
            ));
            $count = $stmt->rowCount();
            $tem = move_uploaded_file($file_data["tmp_name"],$target_dir.$photo_name);
            if($count==1 and $tem)
            {
                echo "<h3>Upload Successful</h3>.<br> <a href='show.php?id=$id' class='btn btn-warning'>Go to Profile</a> ";
            }
            else
            {
                echo "<h3 style='color: red'>Upload Unsuccessful</h3>";
            }


        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
    public function index()
    {
        try {
            $query = "SELECT * FROM users ORDER BY ID ASC ";

            $stmt = $this->conn->query($query);

            $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $data;

        }catch(PDOException $e){
            echo $e->getMessage();
        }

    }
    public function store()
    {
        try {
            $query ="INSERT INTO users(full_name,email,password,birth_date,phone,address,bio,edu_institute,class,subject,photo) VALUES(:name,:email,:pass,:birth,:phone,:address,:bio,:edu,:class,:sub,:photo)";

            $stmt =$this->conn->prepare($query);

            $stmt->execute(array(
                ':name' => $this->uname,
                ':email' => $this->uemail ,
                ':pass' => $this->upassword ,
                ':birth' => $this->ubirthdate,
                ':phone' => $this->uphone ,
                ':address' => $this->uaddress ,
                ':bio' => $this->ubio,
                ':edu' => $this->uedu_insti ,
                ':class' => $this->uclass ,
                ':sub' => $this->usubject ,
                ':photo' =>$this->uphoto



            ));


        }catch(PDOException $e){
            echo $e->getMessage();
        }


    }
    public function edit($id)
    {
        try {
            $query = "SELECT * FROM users WHERE ID =".$id;

            $stmt = $this->conn->query($query);

            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            return $data;


        }catch(PDOException $e){
            echo $e->getMessage();
        }

    }
    public function update($id)
    {
        try {
            $query ="UPDATE users SET full_name=:name,email=:email,password=:pass,birth_date=:birth,phone=:phone,address=:address,bio=:bio,edu_institute=:edu,class=:class,subject=:sub  where id=".$id;

//            echo $this->uname;
//            echo $this->ubio;

            $stmt =$this->conn->prepare($query);

            $stmt->execute(array(
                ':name' => $this->uname,
                ':email' => $this->uemail ,
                ':pass' => $this->upassword ,
                ':birth' => $this->ubirthdate,
                ':phone' => $this->uphone ,
                ':address' => $this->uaddress ,
                ':bio' => $this->ubio,
                ':edu' => $this->uedu_insti ,
                ':class' => $this->uclass ,
                ':sub' => $this->usubject ,

            ));


        }catch(PDOException $e){
            echo $e->getMessage();
        }

    }
    public function delete($id)
    {
        try {
            $query ="delete from users where ID=:id";
            $stmt = $this->conn->prepare($query);
            $stmt ->execute(array(
                ':id' =>$id,
            ));

        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
    public function show($id)
    {
        try {
            $query = "SELECT * FROM users WHERE ID =".$id;

            $stmt = $this->conn->query($query);

            $data = $stmt->fetch(PDO::FETCH_ASSOC);
            return $data;

        }catch(PDOException $e){
            echo $e->getMessage();
        }
    }
}

