<html>
<head>
    <title>Update</title>
    <link href="../css/index_style.css"  type="text/css" rel="stylesheet">
    <script src="../js/jquery-3.5.1.slim.js" type="text/javascript"></script>
    <link href="../css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <script src="../js/bootstrap.min.js" type="text/javascript"></script>

</head>
<body>
<div class="container">
    <div class="div-com" id="div1">
        <h1>CRUD</h1>
    </div>

    <div class="div-com"  id="div2">
        <a href="index.php">Home</a>
        <a href="create.php">CREATE</a>

    </div>
    <?php

        include("../vendor/autoload.php");
        use PROFILE\user;
        $user  = new user\Profile();
        $id = $_GET["id"];
        $data= $user->edit($id);
        if(count($data)>0)
        {
        ?>
    <div class="div-com" id="div3">
        <h4>Change Your Record</h4>


                <table style="width: 90%;margin-bottom:20px;"  align="center" border="2px"  CLASS="table-borderless" >
                    <form method="post" action="update.php">
                        <label for="uid" hidden>ID</label>
                        <input type="hidden" name="uid" id="uid" value='<?=$data["id"] ?>'>
                        <tr style="width: auto">
                            <td> Enter Your Full Name:</td>
                            <td> <input class="form-control" type="text" name="uname" value='<?=$data["full_name"] ?>' > </td>
                        </tr>

                        <tr>
                            <td> Enter Your Email:</td>
                            <td> <input class="form-control" type="email" name="uemail" value='<?=$data["email"] ?>' > </td>
                        </tr>
                        <tr>
                            <td> Enter Your Password:</td>
                            <td> <input class="form-control" type="password" name="upassword" value='<?=$data["password"] ?>' > </td>
                        </tr>
                        <tr>
                            <td> Enter Your Birth Date:</td>
                            <td> <input class="form-control" type="date" name="ubirthdate" value='<?=$data["birth_date"] ?>'  > </td>
                        </tr>
                        <tr>
                            <td> Enter Your Phone:</td>
                            <td> <input class="form-control" type="text" name="uphone" value='<?=$data["phone"] ?>' > </td>
                        </tr>
                        <tr>
                            <td> Enter Your Address:</td>
                            <td> <input class="form-control" type="text" name="uaddress" value='<?=$data["address"] ?>' > </td>
                        </tr>
                        <tr>
                            <td> Education Institute Name:</td>
                            <td> <input class="form-control" type="text" name="uedu_insti" value='<?=$data["edu_institute"] ?>' > </td>
                        </tr>

                        <tr>
                            <td> Class:</td>
                            <td> <input class="form-control" type="text" name="uclass" value='<?=$data["class"] ?>' > </td>
                        </tr>


                        <tr>
                            <td> Current Major or Department:</td>
                            <td> <input class="form-control" type="text" name="usubject" value='<?=$data["subject"] ?>' > </td>
                        </tr>

                        <tr>
                            <td>
                                Tell About Yourself:
                            </td>
                            <td>
                                <textarea name="ubio" cols="68" rows="10"><?=$data["bio"] ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <input class="btn btn-primary" type="submit" value="Update Your Info">
                            </td>
                        </tr>


                    </form>
                    <?php
        }
        else
        {
            Echo "<h4 style='color: red'>No Record Found To Update!!</h4>";
        }
        ?>
                </table>

    </div>


</div>
</body>
</html>

<?php
?>