<html>
<head>
    <title>Home</title>
    <link href="../css/index_style.css"  type="text/css" rel="stylesheet">
    <script src="../js/jquery-3.5.1.slim.js" type="text/javascript"></script>
    <link href="../css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <script src="../js/bootstrap.min.js" type="text/javascript"></script>

</head>
<body>
<div class="container">
    <div class="div-com" id="div1">
        <h1>CRUD</h1>
    </div>

    <div class="div-com"  id="div2">
        <a href="index.php">Home</a>
        <a href="create.php">Create</a>

    </div>

    <div class="div-com" id="div3">
        <h4>All Records</h4>

        <?php

        include("../vendor/autoload.php");
        use PROFILE\user;
        $user  = new user\Profile();
        $row = $user->index();
        //    echo"<pre>";
        //    print_r($data);
        //    echo"</pre>";


        ?>

        <table style="width: 90%"  align="center" border="2px"   >
            <tr>
                <th style="width: 5%;text-align:center;">ID</th>
                <th style="width: 30%;text-align:center;">Name</th>
                <th style="width: 30%;text-align:center;">Email</th>
                <th style="width: 40%;text-align:center;">Action</th>
            </tr>

            <?php
            if(count($row)>0){
            $count =0;
            foreach($row as $data)
            {
                //   echo "<pre>";
//                print_r($data["id"]);
//                echo "</pre>";

                ?>
                <tr>
                    <td style="text-align:center;"><?php echo ++$count;?></td>
                    <td style="text-align:center;"><?php echo $data["full_name"]; ?></td>
                    <td style="text-align:center;"><?php echo $data["email"]; ?></td>

                    <td style="text-align:center;padding-top: 3px;padding-bottom: 3px;">
                        <a class=" btn btn-outline-warning" href='edit.php?id=<?php echo $data["id"]?>'> EDIT</a>
                        <a class="btn btn-outline-info" href="show.php?id=<?php echo $data['id']?>">Details</a>
                        <a class="btn btn-outline-danger" href="delete.php?id=<?php echo $data['id']?>">DELETE</a>
                    </td>
                </tr>
            <?php }}
            else
            {
                Echo "<h4 style='color: red'>No Record Found</h4>";
            }
                ?>

        </table>

    <?php

    ?>

    </div>


</div>
</body>
</html>
