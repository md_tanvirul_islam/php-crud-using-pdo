<html lang="us-EN">
<head>
    <title>Add</title>
    <link href="../css/index_style.css"  type="text/css" rel="stylesheet">
    <script src="../js/jquery-3.5.1.slim.js" type="text/javascript"></script>
    <link href="../css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <script src="../js/bootstrap.min.js" type="text/javascript"></script>

</head>
<body>
<div class="container">
    <div class="div-com" id="div1">
        <h1>CRUD</h1>
    </div>

    <div class="div-com"  id="div2">
        <a href="index.php">Home</a>

    </div>

    <div class="div-com" id="div3">
        <h4>Add Record</h4>
        <table style="width: 90%;margin-bottom:20px;"  align="center" border="2px"  CLASS="table-borderless"  >
            <form method="post" action="store.php" enctype="multipart/form-data">
                <tr style="width: auto">
                    <td> Enter Your Full Name:</td>
                    <td> <input class="form-control" type="text" name="uname" placeholder="Md. Tanvirul Islam" > </td>
                </tr>
                <tr>
                    <td> Enter Your Email:</td>
                    <td> <input class="form-control" type="email" name="uemail" placeholder="something@mail.com" > </td>
                </tr>
                <tr>
                    <td> Enter Your Password:</td>
                    <td> <input class="form-control" type="password" name="upassword" placeholder="********" > </td>
                </tr>
                <tr>
                    <td> Enter Your Birth Date:</td>
                    <td> <input class="form-control" type="date" name="ubirthdate"  > </td>
                </tr>
                <tr>
                    <td> Enter Your Phone:</td>
                    <td> <input class="form-control" type="text" name="uphone" placeholder="+880171700000" > </td>
                </tr>
                <tr>
                    <td> Enter Your Address:</td>
                    <td> <input class="form-control" type="text" name="uaddress" placeholder="206/1 North Shajahanpur,Dhaka-1217" > </td>
                </tr>
                <tr>
                    <td> Education Institute Name:</td>
                    <td> <input class="form-control" type="text" name="uedu_insti" placeholder="Current School or College or University" > </td>
                </tr>

                <tr>
                    <td> Class:</td>
                    <td> <input class="form-control" type="text" name="uclass" placeholder="class 8 or 12 or Honours 2nd" > </td>
                </tr>


                <tr>
                    <td> Current Major or Department:</td>
                    <td> <input class="form-control" type="text" name="usubject" placeholder="science or EEE" > </td>
                </tr>

                <tr>
                    <td> Upload Your Photo:</td>
                    <td> <input class="form-control" type="file" name="uphoto"  > </td>
                </tr>
                <tr>
                    <td>
                        Tell About Yourself:
                    </td>
                    <td>
                        <textarea name="ubio" cols="68" rows="10"></textarea>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input class="btn btn-primary" type="submit" value="ADD YoU Info">
                    </td>
                </tr>


            </form>
        </table>

    </div>


</div>
</body>
</html>

<?php
?>