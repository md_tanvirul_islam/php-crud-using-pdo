<html>
<head>
    <title>Profile</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="../css/profile.css" rel="stylesheet">

</head>
<body>
<?php
include("../vendor/autoload.php");
use PROFILE\user;
$user  = new user\Profile();
$id = $_GET["id"];
$data= $user->show($id);
//echo "<pre>";
//print_r($data);
//echo "<pre>";
?>
    <div class="container emp-profile">
        <form method="post">
            <div class="row">
                <div class="col-md-4">
                    <div class="profile-img">
                        <img src="../assets/photo/profile/<?=$data["photo"]?>" alt="" style="height:150px;width:
   233px;"/>
                        <div class="file btn btn-lg btn-primary">
                            <a style="text-decoration-line: none;color: #ffffff" href="photoedit.php?id=<?php echo $data["id"]?>">Change
                                Photo</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="profile-head">
                        <h5>
                           <?php echo $data["full_name"] ?>
                        </h5>
                        <h6>
                            Web Developer and Designer
                        </h6>
                        <p class="proile-rating">RANKINGS : <span>8/10</span></p>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">About</a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <a class="profile-edit-btn btn btn-info" name="btnAddMore" href="edit.php?id=<?php echo $data["id"]?>"
                       style="text-decoration-line:none;color: #ffffff;"
                    >Edit Profile</a>
                </div>
                <div class="col-md-2">
                    <a class="profile-edit-btn btn btn-secondary" name="btnAddMore" href="index.php"
                       style="text-decoration-line:none;color: #ffffff;"
                    >Home</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="profile-work">
                        <p>SKILLS</p>
                        <a href="">Web Designer</a><br/>
                        <a href="">Web Developer</a><br/>
                        <a href="">WordPress</a><br/>
                        <a href="">WooCommerce</a><br/>
                        <a href="">PHP, .Net</a><br/>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="tab-content profile-tab" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>User Id</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?php echo $data["id"] ?></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Name</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?php echo $data["full_name"] ?></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Email</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?php echo $data["email"] ?></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Phone</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?php echo $data["phone"] ?></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Birth Date</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?php echo $data["birth_date"] ?></p>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <label>Address</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?php echo $data["address"] ?></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Education Institute</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?php echo $data["edu_institute"] ?></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Class</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?php echo $data["class"] ?></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Subject or Major</label>
                                </div>
                                <div class="col-md-6">
                                    <p><?php echo $data["subject"] ?></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Bio</label><br/>
                                    <p><?php echo $data["bio"] ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</body>
</html>